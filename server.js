const express = require('express');
const cors = require('cors');
const mongoose = require('mongoose');
const config = require('./config');
const users = require('./app/users');
const events = require('./app/events');
const friends = require('./app/friends');

const app = express();

require('express-ws')(app);

const port = 8000;

app.use(cors());
app.use(express.json());
app.use(express.static('public'));

mongoose.connect(config.db.url + '/' + config.db.name);

const db = mongoose.connection;

db.once('open', () => {
  console.log('Mongoose connected!');

  app.use('/users', users());
  app.use('/events', events());
  app.use('/friends', friends());

  app.listen(port, () => {
    console.log(`Server started on ${port} port!`);
  });
});
