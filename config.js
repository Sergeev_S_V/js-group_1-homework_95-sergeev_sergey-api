const path = require('path');

const rootPath = __dirname;

module.exports = {
  rootPath,
  uploadPath: path.join(rootPath, '/public/uploads'),
  db: {
    url: 'mongodb://localhost:27017',
    name: 'events'
  },
  facebook: {
    appId: "1903904909901524", // Enter your app ID here
    appSecret: "8be3d584a59cce234a41a60a78787638" // Enter your app secret here
  }
};

