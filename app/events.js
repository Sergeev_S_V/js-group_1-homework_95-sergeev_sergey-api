const express = require('express');
const User = require('../models/User');
const Event = require('../models/Event');
const auth = require('../middleware/auth');

const createRouter = () => {
  const router = express.Router();

  router.post('/', auth, async (req, res) => {
    const eventData = {
      title: req.body.title,
      startDateTime: req.body.startDateTime,
      endDateTime: req.body.endDateTime,
      userId: req.user._id
    };

    const event = new Event(eventData);
    const savedEvent = await event.save();

    return res.send(savedEvent);
  });

  router.get('/', auth, async (req, res) => {
    const friends = await User
      .find({guests: req.user._id});

    const friendsIds = friends.map(friend => friend._id);
    friendsIds.push(req.user._id);

    const events =
      await Event.find({$and: [
            {userId: {$in: friendsIds}},
            {startDateTime: {$gte: new Date()}}
          ]}
        );

    return res.send(events);
  });

  router.delete('/:id', auth, async (req, res) => {
    const id = req.params.id;

    const event = await Event.findOne({_id: id});

    if (JSON.stringify(event.userId) !== JSON.stringify(req.user._id)) {
      return res.status(403).send({message: 'You have not access to delete this event'});
    }

    await Event.deleteOne({_id: id});

    const friends = await User
      .find({guests: req.user._id});

    const friendsIds = friends.map(friend => friend._id);
    friendsIds.push(req.user._id);

    const events = await Event.find({userId: {$in: friendsIds}});

    return res.send({message: 'Event was deleted!', events});
  });

  return router;
};

module.exports = createRouter;