const express = require('express');

const User = require('../models/User');
const auth = require('../middleware/auth');

const createRouter = () => {
  const router = express.Router();

  router.get('/', auth, async (req, res) => {
    const friends = await User.find({guests: req.user._id});

    return res.send(friends);
  });

  router.put('/', auth, async (req, res) => {
    const email = req.query.email;

    let user = await User.findOne({username: email});

    if (!user) {
      return res.send({error: "You sent incorrect email"})
    }

    const isFriend = await User.findOne({guests: {$in: [user._id]}});

    if (isFriend) {
      return res.send({message: 'You already have had this user in our friends list'});
    }

    await User.findOneAndUpdate({_id: req.user._id}, {$push: {guests: user._id}});
    await User.findOneAndUpdate({_id: user._id}, {$push: {guests: req.user._id}});

    const friends = await User.find({guests: req.user._id});

    return res.send({message: 'Success! you added new friend ' + user.displayName, friends})
  });

  router.delete('/:id', auth, async (req, res) => {
    const id = req.params.id;

    await User
      .findOneAndUpdate(
        {_id: req.user._id},
        {$pull: {guests: id}},
        { safe: true }
      );

    await User
      .findOneAndUpdate(
        {_id: id},
        {$pull: {guests: req.user._id}},
        { safe: true }
      );

    const friends = await User.find({guests: req.user._id});

    return res.send({message: 'Friend was deleted from your friends list', friends});
  });

  return router;
};

module.exports = createRouter;